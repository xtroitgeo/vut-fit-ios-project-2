// Solution IOS Project 2, 2.5.2022
// Author: Troitckii Georgii, FIT
//
// Synchronization. Building water.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sys/wait.h>

#include "shared_resources.h"

/**
 * @brief Initialize params are given from user
 * @param n_oxygen           number of oxygen atoms
 * @param n_hydrogen         number of hydrogen atoms
 * @param max_append_time    max time to append to queue
 * @param max_crt_time       max time to create new molecule
 */
void init_params(int n_oxygen, int n_hydrogen, int max_append_time, int max_crt_time)
{
    params.n_oxygen = n_oxygen;
    params.n_hydrogen = n_hydrogen;
    params.max_appending_time = max_append_time;
    params.max_creating_time = max_crt_time;
}

/**
 * @brief Validates parameters given from user
 * @return 0 on success, -1 wrong arguments
 */
int check_params(int argc, char *argv[])
{

    if (argc != 5) {
        fprintf(stderr, "proj2: wrong number of arguments\n");
        return -1;
    }

    for (int i = 1; i < argc; ++i) {
        if (strlen(argv[i]) == 0) {
            fprintf(stderr, "proj2: argument is empty\n");
            return -1;
        }
    }

    for (int i = 1; i < argc; ++i) {
        for (size_t j = 0; j < strlen(argv[i]); ++j) {
            if ( !isdigit(argv[i][j]) ) {
                fprintf(stderr, "proj2: argument isn't a number\n");
                return -1;
            }
        }
    }

    int n_oxygen = atoi(argv[1]);
    int n_hydrogen = atoi(argv[2]);
    int ti = atoi(argv[3]);
    int tb = atoi(argv[4]);

    if (n_oxygen < 0 || n_hydrogen < 0 || ti < 0 || ti > 1000 || tb < 0  || tb > 1000) {
        fprintf(stderr, "proj2: wrong arguments\n");
        return -1;
    }

    init_params(n_oxygen, n_hydrogen, ti, tb);
    return 0;
}


int main(int argc, char *argv[])
{
    if (check_params(argc, argv) == -1) {
        while(wait(NULL) > 0);
        return EXIT_FAILURE;
    }

    /// Initialize shared memory
    if (!init_resources()) {
        fprintf(stderr, "proj2: failed to initialize shared resources\n");
        while(wait(NULL) > 0);
        return EXIT_FAILURE;
    }

    /// Run oxygen processes
    if (!run_oxygen()) {
        fprintf(stderr, "proj2: failed to create oxygen\n");
        kill_processes(oxy_pids, params.n_oxygen);
        kill_processes(hydro_pids, params.n_hydrogen);
        delete_semaphores();
        unmap_shared_memory();
        fclose(fw);
        while(wait(NULL) > 0);
        return EXIT_FAILURE;
    }

    /// Run hydrogen processes
    if (!run_hydrogen()) {
        fprintf(stderr, "proj2: failed to create hydrogen\n");
        kill_processes(hydro_pids, params.n_hydrogen);
        delete_semaphores();
        unmap_shared_memory();
        fclose(fw);
        while(wait(NULL) > 0);
        return EXIT_FAILURE;
    }

    while(wait(NULL) > 0);

    /// Free resources
    delete_semaphores();
    unmap_shared_memory();
    fclose(fw);


    return EXIT_SUCCESS;
}
