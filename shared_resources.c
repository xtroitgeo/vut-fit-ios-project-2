#include <sys/mman.h>
#include <unistd.h>
#include <signal.h>
#include <stdlib.h>
#include <time.h>

#include "shared_resources.h"
#include "actions.h"


/**
 * @brief Initialize shared resources.
 * @return true on success, false on failure
 */
bool init_resources() {

    shared_mem = mmap(NULL, sizeof(shared_resources_t *),
                      PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
    if (shared_mem == MAP_FAILED) {
        goto unmap_shm;
    }

    hydro_pids = mmap(NULL, sizeof(pid_t *) * params.n_hydrogen,
                      PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    if (hydro_pids == MAP_FAILED) {
        goto unmap_hydro;
    }


    oxy_pids = mmap(NULL, sizeof(pid_t *) * params.n_oxygen,
                    PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    if (oxy_pids == MAP_FAILED) {
        goto unmap_all;
    }

    /// Open file to write logs
    fw = fopen("proj2.out", "w");
    if (fw == NULL) {
        goto unmap_all;
    }

    /// Initialize arrays of pid_t to -2
    for (int i = 0; i < params.n_hydrogen; ++i) {
        hydro_pids[i] = -2;
    }

    for (int i = 0; i < params.n_oxygen; ++i) {
        oxy_pids[i] = -2;
    }

    /// Initialize semaphores
    if (sem_init(&shared_mem->mutex, 1, 1) == -1 )       goto del_s1;
    if (sem_init(&shared_mem->hydrogen_sem, 1, 0) == -1) goto del_s2;
    if (sem_init(&shared_mem->oxygen_sem, 1, 0) == -1)   goto del_s3;
    if (sem_init(&shared_mem->print_mutex, 1, 1) == -1)  goto del_s4;
    if (sem_init(&shared_mem->barrier1.turnstile1, 1, 0) == -1)  goto del_s5;
    if (sem_init(&shared_mem->barrier1.turnstile2, 1, 1) == -1)  goto del_s6;
    if (sem_init(&shared_mem->barrier1.mutex, 1, 1) == -1)  goto del_s7;
    if (sem_init(&shared_mem->signal_sem, 1, 0) == -1)  goto del_s8;
    if (sem_init(&shared_mem->barrier2.turnstile1, 1, 0) == -1)  goto del_s9;
    if (sem_init(&shared_mem->barrier2.turnstile2, 1, 1) == -1)  goto del_s10;
    if (sem_init(&shared_mem->barrier2.mutex, 1, 1) == -1)  goto del_s11;

    if (sem_init(&shared_mem->o_wait, 1, 1) == -1)  goto del_s12;
    if (sem_init(&shared_mem->h_wait, 1, 2) == -1)  goto del_s13;


    /// Initialize shared counters to 0
    shared_mem->number_of_line = 0;
    shared_mem->number_hydrogen = 0;
    shared_mem->number_oxygen = 0;
    shared_mem->number_molecule = 1;

    shared_mem->barrier1.counter = 0;
    shared_mem->barrier2.counter = 0;

    shared_mem->number_of_atoms = params.n_hydrogen + params.n_oxygen;
    shared_mem->queue_size = 0;

    shared_mem->free_o = params.n_oxygen;
    shared_mem->free_h = params.n_hydrogen;


    return true;

    /// Unmap shared memory
    unmap_all:
        munmap(oxy_pids, sizeof(pid_t) * params.n_oxygen);
        unmap_hydro: munmap(hydro_pids, sizeof(pid_t) * params.n_hydrogen);
        unmap_shm: munmap(shared_mem, sizeof(shared_resources_t));
        return false;


    /// Destroy semaphores
    del_s13: sem_destroy(&shared_mem->h_wait);
    del_s12: sem_destroy(&shared_mem->o_wait);

    del_s11: sem_destroy(&shared_mem->barrier2.mutex);
    del_s10: sem_destroy(&shared_mem->barrier2.turnstile2);
    del_s9: sem_destroy(&shared_mem->barrier2.turnstile1);

    del_s8: sem_destroy(&shared_mem->signal_sem);
    del_s7: sem_destroy(&shared_mem->barrier1.mutex);
    del_s6: sem_destroy(&shared_mem->barrier1.turnstile2);
    del_s5: sem_destroy(&shared_mem->barrier1.turnstile1);
    del_s4: sem_destroy(&shared_mem->print_mutex);
    del_s3: sem_destroy(&shared_mem->oxygen_sem);
    del_s2: sem_destroy(&shared_mem->hydrogen_sem);
    del_s1: sem_destroy(&shared_mem->mutex);
    return false;
}

/**
 * @brief Run hydrogen processes
 * @return true on success, false on failure
 */
bool run_hydrogen()
{
    for (int i = 0; i < params.n_hydrogen; ++i) {
        hydro_pids[i] = fork();
        if (hydro_pids[i] == -1) {
            return false;
        }
        else if (hydro_pids[i] == 0) {
            srand(time(NULL) * getpid());
            create_hydrogen(i + 1);
            fclose(fw);
            exit(EXIT_SUCCESS);
        }
    }

    return true;
}

/**
 * @brief Run oxygen processes
 * @return true on success, false on failure
 */
bool run_oxygen()
{
    for (int i = 0; i < params.n_oxygen; ++i) {
        oxy_pids[i] = fork();
        if (oxy_pids[i] == -1) {
            return false;
        }
        else if (oxy_pids[i] == 0) {
            srand(time(NULL) * getpid());
            create_oxygen(i + 1);
            fclose(fw);
            exit(EXIT_SUCCESS);
        }
    }

    return true;
}


/**
 * @brief Freeing forked processes in case of error
 * @param pids        array of pids
 * @param n_processes number of processes
 */
void kill_processes(pid_t* pids, int n_processes)
{
    for(int i = 0; i < n_processes && pids[i] != -2; i++)
        kill(pids[i], SIGTERM);
}

/**
 * @brief Destroy semaphores
 */
void delete_semaphores()
{
    sem_destroy(&shared_mem->barrier1.mutex);
    sem_destroy(&shared_mem->barrier1.turnstile2);
    sem_destroy(&shared_mem->barrier1.turnstile1);
    sem_destroy(&shared_mem->print_mutex);
    sem_destroy(&shared_mem->oxygen_sem);
    sem_destroy(&shared_mem->hydrogen_sem);
    sem_destroy(&shared_mem->mutex);
}

/**
 * @brief Freeing shared memory
 */
void unmap_shared_memory()
{
    munmap(shared_mem, sizeof(shared_resources_t));
    munmap(oxy_pids, sizeof(pid_t) * params.n_oxygen);
    munmap(hydro_pids, sizeof(pid_t) * params.n_hydrogen);
}
