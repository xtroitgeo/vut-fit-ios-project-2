#include <semaphore.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "actions.h"
#include "shared_resources.h"

/**
 * @brief Services the hydrogen process
 * @param id
 */
void create_hydrogen(int id)
{
    enum status state = NONE;

    print_log("H", id, "started", state);

    while(true)
    {

        usleep(1000 * (rand() % (params.max_appending_time + 1)));

        /// going to queue
        print_log("H", id, "going to queue", state);

        /// Two hydrogen atoms have access to critical section at one time
        sem_wait(&shared_mem->h_wait);


        /// Check if enough atoms are available for creating a new water molecule
        // =============================================================================
        if (shared_mem->free_h < 2 || shared_mem->free_o < 1) {

            /// Before printing "not enough" message, check if there are all atoms in queue
            if (shared_mem->queue_size != (shared_mem->free_h + shared_mem->free_o)){
                sem_wait(&shared_mem->signal_sem);
            }

            print_log("H", id, "not enough O or H", state);

            sem_post(&shared_mem->h_wait);
            return;
        }
        // =============================================================================

        sem_wait(&shared_mem->mutex);

        shared_mem->number_hydrogen++;

        /// If number of atoms is enough, unlock semaphores
        if (shared_mem->number_hydrogen >= 2 && shared_mem->number_oxygen >= 1) {
            sem_post(&shared_mem->hydrogen_sem);
            sem_post(&shared_mem->hydrogen_sem);
            shared_mem->number_hydrogen -= 2;

            sem_post(&shared_mem->oxygen_sem);
            shared_mem->number_oxygen--;
        }
        else {
            sem_post(&shared_mem->mutex);
        }

        sem_wait(&shared_mem->hydrogen_sem);

        /// Create molecule
        state = PREP;
        print_log("H", id, "creating molecule", state);

        /// Reusable barrier1
        barrier1(); // wait for 3 molecules (H-O-H)

        state = READY;
        print_log("H", id, "", state);
        barrier2(); // inc molecule counter

        sem_post(&shared_mem->h_wait);
        break;
    }
}

/**
 * @brief Services the oxygen process
 * @param id
 */
void create_oxygen(int id)
{
    enum status state = NONE;

    /// Started
    print_log("O", id, "started", state);

    while(true)
    {
        usleep(1000 * (rand() % (params.max_appending_time + 1)));

        /// Going to Queue
        print_log("O", id, "going to queue", state);

        /// One oxygen atom has access to critical section at one time
        sem_wait(&shared_mem->o_wait);

        /// Check if enough atoms are available for creating a new water molecule
        /// ==========================================================================
        if (shared_mem->free_h < 2 || shared_mem->free_o < 1) {

            sem_post(&shared_mem->o_wait);

            /// Before printing "not enough" message, check if there are all atoms in queue
            if (shared_mem->queue_size != (shared_mem->free_h + shared_mem->free_o)) {
                sem_wait(&shared_mem->signal_sem);
            }

            print_log("O", id, "not enough H", state);
            return;
        }

        /// ==========================================================================


        sem_wait(&shared_mem->mutex);
        shared_mem->number_oxygen++;


        if (shared_mem->number_hydrogen >= 2) {
            sem_post(&shared_mem->hydrogen_sem);
            sem_post(&shared_mem->hydrogen_sem);
            shared_mem->number_hydrogen -= 2;

            sem_post(&shared_mem->oxygen_sem);
            shared_mem->number_oxygen--;
        }
        else {
            sem_post(&shared_mem->mutex);
        }

        sem_wait(&shared_mem->oxygen_sem);

        /// Create molecule
        state = PREP;
        print_log("O", id, "creating molecule", state);


        /// Simulation of molecule creating
        usleep(1000 * (rand() % (params.max_creating_time + 1)));

        /// Reusable barrier1
        barrier1(); // wait for 3 molecules (H-O-H)

        state = READY;
        print_log("O", id, "", state);
        barrier2(); // inc molecule counter

        sem_post(&shared_mem->o_wait);
        sem_post(&shared_mem->mutex);
        break;
    }
}

/**
 * @brief Prints log messages to file
 * @param id
 */
void print_log(char* type, int id, char* action, enum status actual_state)
{
    sem_wait(&shared_mem->print_mutex);

    shared_mem->number_of_line++;

    /// 'stared' and 'going to queue' messages
    if (actual_state == NONE) {
        if (strncmp(action, "going", 5) == 0) {
            shared_mem->queue_size++; // increment actual queue size

            /// If all atoms are in queue, signal that they can print "not enough" message
            if (shared_mem->queue_size == shared_mem->number_of_atoms) {
                for (int i = 0 ; i < shared_mem->number_of_atoms; i++) {
                    sem_post(&shared_mem->signal_sem);
                }
            }
        }
        fprintf(fw, "%d: %s %d: %s\n",
                shared_mem->number_of_line, type, id, action);
    }
    else if (actual_state == PREP) {
        fprintf(fw, "%d: %s %d: %s %d\n",
                shared_mem->number_of_line, type, id, action, shared_mem->number_molecule);
    }
    else if (actual_state == READY) {
        fprintf(fw, "%d: %s %d: molecule %d created\n",
                shared_mem->number_of_line, type, id, shared_mem->number_molecule);

        /// Decrement number of molecules
        if (*type == 'O')
            shared_mem->free_o--;

        else if (*type == 'H')
            shared_mem->free_h--;
    }

    fflush(fw);

    sem_post(&shared_mem->print_mutex);
}


void barrier1()
{
    sem_wait(&shared_mem->barrier1.mutex);
        int count = ++shared_mem->barrier1.counter;
        if (count == 3) {
            sem_wait(&shared_mem->barrier1.turnstile2);
            sem_post(&shared_mem->barrier1.turnstile1);
        }
    sem_post(&shared_mem->barrier1.mutex);

    sem_wait(&shared_mem->barrier1.turnstile1);
    sem_post(&shared_mem->barrier1.turnstile1);

    sem_wait(&shared_mem->barrier1.mutex);
        count = --shared_mem->barrier1.counter;

        if (count == 0) {
            sem_wait(&shared_mem->barrier1.turnstile1);
            sem_post(&shared_mem->barrier1.turnstile2);
        }
    sem_post(&shared_mem->barrier1.mutex);

    sem_wait(&shared_mem->barrier1.turnstile2);
    sem_post(&shared_mem->barrier1.turnstile2);
}

void barrier2()
{
    sem_wait(&shared_mem->barrier1.mutex);
    int count = ++shared_mem->barrier1.counter;
    if (count == 3) {
        shared_mem->number_molecule++;
        sem_wait(&shared_mem->barrier1.turnstile2);
        sem_post(&shared_mem->barrier1.turnstile1);
    }
    sem_post(&shared_mem->barrier1.mutex);

    sem_wait(&shared_mem->barrier1.turnstile1);
    sem_post(&shared_mem->barrier1.turnstile1);


    sem_wait(&shared_mem->barrier1.mutex);
        count = --shared_mem->barrier1.counter;

    if (count == 0) {
        sem_wait(&shared_mem->barrier1.turnstile1);
        sem_post(&shared_mem->barrier1.turnstile2);
    }
    sem_post(&shared_mem->barrier1.mutex);

    sem_wait(&shared_mem->barrier1.turnstile2);
    sem_post(&shared_mem->barrier1.turnstile2);
}

