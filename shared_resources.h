#ifndef SHARED_RESOURCES_H
#define SHARED_RESOURCES_H

#include <stdio.h>
#include <stdbool.h>
#include <semaphore.h>
#include "shared_structures.h"


FILE *fw;
shared_resources_t *shared_mem;
params_t params;
pid_t *hydro_pids;
pid_t *oxy_pids;


bool init_resources();
bool run_hydrogen();
bool run_oxygen();

// Resource freeing
void delete_semaphores();
void unmap_shared_memory();
void kill_processes(pid_t* pids, int n_processes);


#endif //SHARED_RESOURCES_H
