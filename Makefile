CC = gcc
CFLAGS = -std=gnu99 -Wall -Wextra -Werror -pedantic -g
LD = -lpthread

OBJS = proj2.o shared_resources.o actions.o
OUT = proj2

.PHONY: clear clean all

all: proj2

proj2: $(OBJS)
	$(CC) $(CFLAGS) $^ -o $@ $(LD)

clean:
clear:
	rm -rf $(OBJS) $(OUT) proj2.out
