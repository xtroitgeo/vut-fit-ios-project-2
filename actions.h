#ifndef ACTIONS_H
#define ACTIONS_H

/// Status of molecule creation
enum status {NONE, PREP, READY};

void create_oxygen(int id);
void create_hydrogen(int id);
void print_log(char* type, int id, char* action, enum status actual_state);

void barrier1();
void barrier2();

#endif //ACTIONS_H
