#ifndef SHARED_STRUCTURES_H
#define SHARED_STRUCTURES_H

#include <semaphore.h>

typedef struct {
    int n_oxygen;
    int n_hydrogen;
    int max_appending_time;
    int max_creating_time;
} params_t;


typedef sem_t Semaphore;

typedef struct {
    Semaphore turnstile1;
    Semaphore turnstile2;
    Semaphore mutex;
    int counter;
} Barrier;

typedef struct {

    int number_oxygen;
    int number_hydrogen;
    int number_of_line;
    int number_molecule;

    int number_of_atoms;
    int queue_size;

    int free_o;
    int free_h;

    Semaphore o_wait;
    Semaphore h_wait;
    Semaphore mutex;
    Semaphore oxygen_sem;
    Semaphore hydrogen_sem;
    Semaphore print_mutex;
    Semaphore signal_sem;
    Barrier barrier1;
    Barrier barrier2;

} shared_resources_t;

#endif //SHARED_STRUCTURES_H
