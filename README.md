# IOS - Operating systems
# Project 2
[Assignment](assignment.pdf)  in Czech.

### Task details
- The assignment is inspired by the book Allen B. The Little Book of Semaphores (Building H2O)
- Water molecules are made of two hydrogen atoms and one oxygen atom. There are three types in the system
  processes: the main process, oxygen and hydrogen. 
- Once the processes are formed, the processes representing oxygen and hydrogen are arranged in two queues for oxygen and one for hydrogen. 
- Always one oxygen and two hydrogen step out to form a molecule. Only one molecule can be created at a time. 
- After molecule is formed, space is made available for other atoms to form the next molecule.
- When there are no longer enough oxygen atoms available or hydrogen atoms for the next molecule (and no more will be created by the main process) are \
  all remaining oxygen and hydrogen atoms are released from the queues and the processes are terminated.



### Run:
./proj2 NO NH TI TB
- NO: Number of oxygen
- NH: Number of hydrogen
- TI: Maximum time in milliseconds (0<=TI<=1000) that an oxygen/hydrogen atom waits after its creation 
- TB: Maximum time in milliseconds (0<=TB<=1000) to create one molecule

## Evaluation 15/15


